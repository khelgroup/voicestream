var port = 3000;
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http); // socket.io instance initialization

// listen on the connection event for incoming sockets
io.on('connection', function (socket) {
    console.log('A new client connected')

    socket.on('radio', function(blob) {
        console.log('see', blob);
        // can choose to broadcast it to whoever you want
        socket.broadcast.emit('voice', blob);
    });

   /* // To subscribe the socket to a given channel
    socket.on('join', function (data) {
        socket.join(data.username);
    });

    // To keep track of online users
    socket.on('userPresence', function (data) {
        onlineUsers[socket.id] = {
            username: data.username
        };
        socket.broadcast.emit('onlineUsers', onlineUsers);
    });

    // For message passing
    socket.on('message', function (data) {
        io.sockets.to(data.toUsername).emit('message', data.data);
    });

    // To listen for a client's disconnection from server and intimate other clients about the same
    socket.on('disconnect', function (data) {
        socket.broadcast.emit('disconnected', onlineUsers[socket.id].username);

        delete onlineUsers[socket.id];
        socket.broadcast.emit('onlineUsers', onlineUsers);
    });*/
});

http.listen(port, function () {
    console.log('Signaler listening on *: ', port);
});
